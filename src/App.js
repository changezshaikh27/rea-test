import React, { Component } from 'react';
import './App.css';
import PropertyListings from './components/property-listings/property-listings';
import Properties from './assets/data/properties.json';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="container">
          <div className="row">
            <div className="col-sm-12">
              <PropertyListings properties={Properties} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
