import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import * as Constants from '../../shared/Constants';

//#region Property Tile Styles

const PropertyFooter = styled.div`
  display: flex;
  padding: 10px;
  height: 50px;
  align-items: center;
  justify-content: space-between;
  @media ${Constants.device.laptop} {
    button {
      display: none;
    }
  }
`;

const PropertyTileContainer = styled.div`
  border-radius: 5px;
  margin-bottom: 20px;
  border: solid 1px #e7e6e6;
  box-shadow: 0 1px 2px 0 rgba(0,0,0,.2);
  &:hover {
    ${PropertyFooter} button{
      display: block;
    }
  }
`;

const PropertyHeader = styled.div`
  display: flex;
  align-items: center;
  padding: 10px;
  background-color: ${props => props.bgcolor};
`;


const LogoImage = styled.img`
  margin: 10px 0;
`;

const PropertyImage = styled.img`
  width: 100%;
  object-fit: contain;
`;

const PropertyPrice = styled.span`
  font-weight: 600;
  font-size: 20px;
`;

//#endregion

export default class PropertyTile extends React.Component {

  constructor(props) {
    super(props);
    this.renderActionButton = this.renderActionButton.bind(this);
  }

  handlePropertyAction = () => {
    const isSavedProperty = this.props.propertyType === Constants.PROPERTY_TYPES.SAVED;
    if(!isSavedProperty) {
      this.props.saveProperty(this.props.propertyDetail);
    } else {
      this.props.removeProperty(this.props.propertyDetail);
    }
  }

  renderActionButton() {
    const isSavedProperty = this.props.propertyType === Constants.PROPERTY_TYPES.SAVED;
    const buttonText = isSavedProperty ? 'Remove Property' : 'Save Property';
    const buttonClass = isSavedProperty ? 'btn btn-danger' : 'btn btn-primary';
    return (
      <button type="button" 
              className={buttonClass} 
              onClick={this.handlePropertyAction}>
              {buttonText}
      </button>
    )
  }

  render() {
    // get property detail from props
    const property = this.props.propertyDetail;
    return (
      <PropertyTileContainer>
        <PropertyHeader bgcolor={property.agency.brandingColors.primary}>
          <LogoImage src={property.agency.logo} />
        </PropertyHeader>
        <PropertyImage
          src={property.mainImage}
          alt={property.id}
        />
        <PropertyFooter>
          <PropertyPrice>{property.price}</PropertyPrice>
          {this.renderActionButton()}
        </PropertyFooter>
      </PropertyTileContainer>
    );
  }
}

PropertyTile.propTypes = {
  propertyDetail: PropTypes.object.isRequired,
  propertyType: PropTypes.string.isRequired,
  saveProperty: PropTypes.func.isRequired,
  removeProperty: PropTypes.func.isRequired
};
