import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import PropertyTile from '../property-tile/property-tile';
import * as Constants from '../../shared/Constants';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

//#region Property Listings Styles

const PropertyContainer = styled.div`
margin-top: 10px;`;

const ColumnHeader = styled.h2`
  text-align: center;
`;

//#endregion

/**
 * The Property Listings component, contains the properties
 * 
 * @class PropertyListings
 * @extends {React.Component}
 */
export default class PropertyListings extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      results: this.props.properties.results || [],
      savedProperties: this.props.properties.saved || []
    }
    this.renderPropertyList = this.renderPropertyList.bind(this);
    this.saveProperty = this.saveProperty.bind(this);
    this.removeProperty = this.removeProperty.bind(this);
  }

  /**
   * Takes in a property and saves it in the Saved Properties list
   * 
   * @param {any} property 
   * @memberof PropertyListings
   */
  saveProperty(property) {
    // create a local copy of the saved properties
    const savedProperties = this.state.savedProperties;
    // do nothing if the property already exists as a saved property
    if(savedProperties.includes(property)) return;

    savedProperties.push(property);

    // update the state with the new list
    this.setState({savedProperties});
  }

  /**
   * Takes in a property and removes it from the saved properties list
   * 
   * @param {any} propertyToRemove 
   * @returns 
   * @memberof PropertyListings
   */
  removeProperty(propertyToRemove) {
    // create a local copy of the saved properties
    let savedProperties = this.state.savedProperties;
    // do nothing if the property does not already exist as a saved property
    if(!savedProperties.includes(propertyToRemove)) return;

    savedProperties = savedProperties.filter(property => property !== propertyToRemove);

    // update the state with the new list
    this.setState({savedProperties});
  }

  /**
   * Renders the Property List
   * 
   * @returns 
   * @memberof PropertyListings
   */
  renderPropertyList(propertyType) {
    const properties = propertyType === Constants.PROPERTY_TYPES.RESULTS ? 
                        this.state.results : 
                        this.state.savedProperties;

    if (!properties || !properties.length) return '';
    return properties.map(property => {
      return (
        <PropertyTile propertyDetail={property} 
                      key={property.id} 
                      propertyType={propertyType}
                      saveProperty={this.saveProperty}
                      removeProperty={this.removeProperty} />
      )
    });
  }

  render() {
    return (
      <section className="property-listings">
        <PropertyContainer className="container">
          <div className="row">
            <div className="col-xs-12 col-md-6">
              <ColumnHeader>Results</ColumnHeader>
                {this.renderPropertyList(Constants.PROPERTY_TYPES.RESULTS)}
            </div>
            <div className="col-xs-12 col-md-6">
              <ColumnHeader>Saved Properties</ColumnHeader>
              <ReactCSSTransitionGroup
                transitionName="tiles"
                transitionEnterTimeout={500}
                transitionLeaveTimeout={300}>
                {this.renderPropertyList(Constants.PROPERTY_TYPES.SAVED)}
              </ReactCSSTransitionGroup>
            </div>
          </div>
        </PropertyContainer>
      </section>
    )
  }
}

PropertyListings.propTypes = {
  properties: PropTypes.object.isRequired
}