import React from 'react';
import ReactDOM from 'react-dom';
import {shallow} from 'enzyme';
import PropertyListings from './property-listings';
import Properties from '../../assets/data/properties.json';

describe('Product Grid Tests', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<PropertyListings properties={Properties} />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  test('it should correctly add the property to the saved property list', () => {
    const propertyListings = shallow(<PropertyListings properties={Properties} />);
    // filter results to not contain any properties not already in the saved list
    const results = propertyListings.state().results.filter(property => !propertyListings.state().savedProperties.includes(property));
    if (!results.length) return;
    // get the first property from the results
    const property = results[0];
    // call the save property method
    propertyListings.instance().saveProperty(property);
    // expect the newly added property to be present in the saved properties list
    expect(propertyListings.state().savedProperties.includes(property)).toBe(true);
  });

  test('it should correctly remove the property from the saved property list', () => {
    const propertyListings = shallow(<PropertyListings properties={Properties} />);
    
    // if no properties exist in the saved properties list, add one first
    if(!propertyListings.state().savedProperties.length) {
      const property = propertyListings.state().results[0];
      propertyListings.instance().saveProperty(property);
    }
    
    // get the first property from the saved list
    const property = propertyListings.state().savedProperties[0];
    // call the remove property method
    propertyListings.instance().removeProperty(property);
    // expect the removed property to not be present in the saved properties list
    expect(propertyListings.state().savedProperties.includes(property)).toBe(false);
  });

});